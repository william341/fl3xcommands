package net.pl3x.pl3xcommands;

import java.util.Arrays;
import java.util.List;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xCommands.MyPlugin;

public enum Alias {
	BACK("back", ""),
	BUTCHER("butcher", ""),
	CLEARINVENTORY("clearinventory", "ci,clearinv"),
	COORDS("coords", "pos,position,loc,location,coordinates"),
	DELHOME("delhome", "dhome,rmhome,deletehome,removehome"),
	DELWARP("delwarp", "deletewarp"),
	FLY("fly", ""),
	FLYSPEED("flyspeed", "fs"),
	GOD("god", "tgm,godmode"),
	HAT("hat", "helmet"),
	HOME("home", ""),
	LISTHOMES("listhomes", "lshome,listhome,homes,lshomes,listhomes"),
	LORE("lore", ""),
	MAIL("mail", "email"),
	MOTD("motd", "messageoftheday"),
	NEAR("near", ""),
	NICK("nick", "nickname,displayname,name,changename"),
	PING("ping", "pong"),
	PL3XCOMMANDS("pl3xcommands", ""),
	POWERTOOL("powertool", "pt,assign"),
	RENAME("rename", ""),
	REPAIR("repair", "fix"),
	RULES("rules", "serverrules"),
	SETHOME("sethome", "shome"),
	SETSPAWN("setspawn", ""),
	SETWARP("setwarp", ""),
	SETWORLDSPAWN("setworldspawn", ""),
	SMITE("smite", "strike,lightning"),
	SPAWN("spawn", ""),
	SUDO("sudo", "force,simonsays"),
	SUICIDE("suicide", ""),
	TOP("top", ""),
	TELEPORT("teleport", "tp,port"),
	TP2P("tp2p", "teleportplayertoplayer,teleportplayer2player,tpp2p,teleportp2p,teleportptop"),
	TPA("tpa", "tpr,teleportrequest,tprequest,teleportr"),
	TPAALL("tpaall", "tprall,teleportaskall,teleportrequestall"),
	TPACCEPT("tpaccept", "tpallow,teleportallow,teleportaccept,teleportyes,tpyes,tpac"),
	TPAHERE("tpahere", "tprh,tpah,teleportrequesthere,teleportrhere,tphrequest,tphererequest,tprhere"),
	TPALL("tpall", "teleportall,teleporteveryone"),
	TPDENY("tpdeny", "teleportdeny,teleportno,tpno,tpde"),
	TPHERE("tphere", "teleporthere,tph"),
	TPPOS("tppos", "tpos,teleportposition,tpcoords,tpp,tpc,teleportcoordinates,teleportpos,tpposition,tposition"),
	TPTOGGLE("tptoggle", "teleporttoggle,toggletp,toggleteleport"),
	WALKSPEED("walkspeed", "ws"),
	WARP("warp", ""),
	WORLD("world", "dimension");

	private String key;
	private String def;

	private static BaseConfig config;

	private Alias(String key, String def) {
		this.key = key;
		this.def = def;
		init();
	}

	private static void init() {
		config = new BaseConfig(MyPlugin.class, MyPlugin.getInstance().getPluginInfo(), "", "aliases.ini");
		config.load();
	}

	public static void refreshAll() {
		config = null;
		init();
	}

	public List<String> get() {
		String value = config.get(key, def).replace(" ", "");
		if (value == null || value.equals("")) {
			// This shouldn't happen
			return Arrays.asList(new String[] {});
		}
		return Arrays.asList(value.split(","));
	}
}
