package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.server.MinecraftServer;
import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.TextFile;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class MOTD implements MC_Command {
	private MyPlugin plugin;

	public MOTD(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public static List<String> replaceVars(MyPlugin plugin, List<String> motd, MC_Player player) {
		List<String> list = new ArrayList<String>();
		for (String line : motd) {
			list.add(line.replace("{player}", player == null ? "console" : player.getCustomName()).replace("{online}", Integer.toString(plugin.getServer().getPlayers().size())).replace("{maxplayers}", Integer.toString(MinecraftServer.getServer().H())));
		}
		return list;
	}

	@Override
	public List<String> getAliases() {
		return Alias.MOTD.get();
	}

	@Override
	public String getCommandName() {
		return "motd";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/motd - " + Lang.MOTD_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7motd &a- &d" + Lang.MOTD_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		List<String> motd = TextFile.getLineByLine(plugin, "motd.txt");
		if (motd == null || motd.isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.MOTD_NO_MOTD.get());
		}
		for (String line : MOTD.replaceVars(plugin, motd, player)) {
			Pl3xLibs.sendMessage(player, line);
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.motd");
	}
}
