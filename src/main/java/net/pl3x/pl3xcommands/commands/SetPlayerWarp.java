package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class SetPlayerWarp implements MC_Command {
	private MyPlugin plugin;

	public SetPlayerWarp(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.SETWARP.get();
	}

	@Override
	public String getCommandName() {
		return "setwarp";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7setwarp &e<&7warp&e> &a- &d" + Lang.SETWARP_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.SETWARP_NO_NAME.get());
			return;
		}
		String warpName = args[0].trim().toLowerCase();
		plugin.getWarpConfig().addWarp(warpName, player.getLocation());
		Pl3xLibs.sendMessage(player, Lang.SETWARP_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.setwarp");
	}
}
