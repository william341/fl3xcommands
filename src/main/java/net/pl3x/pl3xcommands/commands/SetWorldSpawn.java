package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class SetWorldSpawn implements MC_Command {
	public SetWorldSpawn(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.SETWORLDSPAWN.get();
	}

	@Override
	public String getCommandName() {
		return "setworldspawn";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		return Pl3xLibs.colorize("&e/&7setworldspawn &a- &d" + Lang.SETWORLDSPAWN_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		MC_World world = player.getWorld();
		Pl3xLibs.setWorldSpawn(world, player.getLocation());
		Pl3xLibs.sendMessage(player, Lang.SETWORLDSPAWN_SET.get().replace("{world}", Pl3xLibs.getWorldName(world)));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.setworldspawn");
	}
}
