package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tp2p implements MC_Command {
	private MyPlugin plugin;

	public Tp2p(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TP2P.get();
	}

	@Override
	public String getCommandName() {
		return "tp2p";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (player == null) {
			return Pl3xLibs.decolorize("/tp2p <player> <player> - " + Lang.TP2P_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7tp2p &e<&7player&e> <&7player&e> &a- &d" + Lang.TP2P_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 2) {
			Pl3xLibs.sendMessage(player, Lang.TP2P_NO_PLAYER.get());
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		MC_Player destination = Pl3xLibs.getPlayer(args[1]);
		if (target == null || destination == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if ((PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled") || PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled")) && !player.hasPermission("command.tp.override")) {
			Pl3xLibs.sendMessage(player, Lang.TP_PLAYER_DISABLED_TP.get());
		}
		target.teleport(destination.getLocation());
		Pl3xLibs.sendMessage(player, Lang.TP2P_TELEPORTED.get().replace("{player}", target.getCustomName()).replace("{destination}", destination.getCustomName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.tp2p");
	}
}
